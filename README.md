# payment-gateway
You need to run payment-webapp application as frontend module

# Getting Started
Set SDK for project (JDK 13)

## IDEA configuration
Install plugin "Lombok" to your editor (for older IDEA version)

## API documentation
* Open API doc via Swagger http://localhost:8081/swagger-ui/

## DB CONFIGURATION
After running the application pls navigate to http://localhost:8081/h2-console
   
   DriverClassName: org.h2.Driver
   JDBC URL: jdbc:h2:file:~/transaction
   - UserName: username
   - Password: password
   
## Task
Create a small backend api (written in Java, using Spring Boot, Gradle and MyBatis).

All other choices of technologies are up to you.

The application needs to support the following functionalities:

	• Searching and viewing detailed information of customers and payments/transactions of their accounts

	• Making transactions between customers’ accounts

A “customer” is defined as a person that is a client of the bank.
A customer can have one or multiple bank “accounts” in the bank to hold their money.
A “transaction“ is the movement of money between any two accounts.
Feel free to come up with missing attributes (from your own experience) and add them to the database model.
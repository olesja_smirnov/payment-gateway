package com.payment.domain.dao;

import java.util.List;
import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.payment.domain.model.Customer;

@Mapper
@Repository
public interface CustomerMapper {

    Optional<Customer> getCustomerById(@Param("id") Long id);

    List<Customer> getAllCustomers();

    void updateCustomer(Customer customer);

    List<Customer> searchCustomer(@Param("searchParam") String searchParam);

}
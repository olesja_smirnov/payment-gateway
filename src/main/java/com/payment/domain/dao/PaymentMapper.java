package com.payment.domain.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.payment.domain.model.Payment;
import com.payment.web.request.PaymentRequest;

@Mapper
@Repository
public interface PaymentMapper {

    List<Payment> getTransactionsByCustomerAccountId(@Param("accountId") Long accountId);

    void arrangeTransactionBetweenCustomers(PaymentRequest request);

}
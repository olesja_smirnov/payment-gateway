package com.payment.domain.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.payment.domain.model.Account;

@Mapper
@Repository
public interface AccountMapper {

    List<Account> getCustomerAccounts(@Param("customerId") Long customerId);

    Optional<Account> getAccountById(@Param("id") Long id);

    void updatePayerAccountBalance(Long payerAccountId, BigDecimal amount);
    void updateReceiverAccountBalance(Long receiverAccountId, BigDecimal amount);

}
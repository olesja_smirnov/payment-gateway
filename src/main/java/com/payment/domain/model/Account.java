package com.payment.domain.model;

import java.math.BigDecimal;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class Account extends AuditModel {

    private Long id;

    @ManyToOne
    private Customer customer;

    private Long accountNumber;

    private BigDecimal balance;

    @Builder.Default
    private String currency = "EUR";

}
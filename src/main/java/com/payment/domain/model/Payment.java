package com.payment.domain.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class Payment extends AuditModel {

    private Long id;

    @OneToOne
    private Customer payer;

    @OneToOne
    private Account payerAccount;

    @OneToOne
    private Customer receiver;

    @OneToOne
    private Account receiverAccount;

    private String subject;

    private BigDecimal amount;

    @Builder.Default
    private String currency = "EUR";

    private Long refNumber;

    private LocalDateTime timestamp;

}
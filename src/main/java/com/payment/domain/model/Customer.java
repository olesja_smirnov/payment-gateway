package com.payment.domain.model;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class Customer extends AuditModel {

    private Long id;

    private String firstName;

    private String lastName;

    private String personalCode;

    @Builder.Default
    private List<Account> accounts = new ArrayList<>();

}
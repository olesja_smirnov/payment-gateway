package com.payment.responseMapper;

import org.dozer.DozerConverter;
import org.springframework.stereotype.Service;
import com.payment.domain.model.Account;
import com.payment.web.response.AccountResponse;

@Service
public class AccountResponseMapper extends DozerConverter<Account, AccountResponse> {

    public AccountResponseMapper() {
        super(Account.class, AccountResponse.class);
    }

    @Override
    public AccountResponse convertTo(Account source, AccountResponse destination) {

        return AccountResponse.builder()
                .id(source.getId())
                .accountNumber(source.getAccountNumber())
                .balance(source.getBalance())
                .currency(source.getCurrency())
                .build();
    }

    @Override
    public Account convertFrom(AccountResponse source, Account destination) {
        return null;
    }

}
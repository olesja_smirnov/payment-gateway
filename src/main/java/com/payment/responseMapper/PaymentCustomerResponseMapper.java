package com.payment.responseMapper;

import org.dozer.DozerConverter;
import org.springframework.stereotype.Service;
import com.payment.domain.model.Customer;
import com.payment.web.response.PaymentCustomerResponse;

@Service
public class PaymentCustomerResponseMapper extends DozerConverter<Customer, PaymentCustomerResponse> {

    public PaymentCustomerResponseMapper() {
        super(Customer.class, PaymentCustomerResponse.class);
    }


    @Override
    public PaymentCustomerResponse convertTo(Customer source, PaymentCustomerResponse destination) {

        return PaymentCustomerResponse.builder()
                .id(source.getId())
                .firstName(source.getFirstName())
                .lastName(source.getLastName())
                .personalCode(source.getPersonalCode())
                .build();
    }

    @Override
    public Customer convertFrom(PaymentCustomerResponse source, Customer destination) {
        return null;
    }

}
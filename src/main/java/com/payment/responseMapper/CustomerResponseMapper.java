package com.payment.responseMapper;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.payment.domain.dao.AccountMapper;
import com.payment.domain.model.Account;
import com.payment.domain.model.Customer;
import com.payment.web.response.AccountResponse;
import com.payment.web.response.CustomerResponse;

@Service
public class CustomerResponseMapper extends DozerConverter<Customer, CustomerResponse> {

    public CustomerResponseMapper() {
        super(Customer.class, CustomerResponse.class);
    }

    @Autowired
    private AccountResponseMapper accountResponseMapper;

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public CustomerResponse convertTo(Customer customer, CustomerResponse customerResponse) {

        return CustomerResponse.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .personalCode(customer.getPersonalCode())
                .accounts(getAccounts(customer.getId()))
                .build();
    }

    private List<AccountResponse> getAccounts(Long id) {
        List<Account> accounts = accountMapper.getCustomerAccounts(id);

        return accounts.stream()
                        .map(accountResponseMapper::convertTo)
                                .collect(Collectors.toList());
    }

    @Override
    public Customer convertFrom(CustomerResponse customerResponse, Customer customer) {
        return null;
    }

}
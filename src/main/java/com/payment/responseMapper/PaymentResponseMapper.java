package com.payment.responseMapper;

import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.payment.domain.model.Customer;
import com.payment.domain.model.Payment;
import com.payment.web.response.PaymentCustomerResponse;
import com.payment.web.response.PaymentResponse;

@Service
public class PaymentResponseMapper extends DozerConverter<Payment, PaymentResponse> {

    public PaymentResponseMapper() {
        super(Payment.class, PaymentResponse.class);
    }

    @Autowired
    private PaymentCustomerResponseMapper paymentCustomerResponseMapper;

    @Override
    public PaymentResponse convertTo(Payment source, PaymentResponse destination) {

        return PaymentResponse.builder()
                .id(source.getId())
                .payer(getPaymentCustomer(source.getPayer()))
                .receiver(getPaymentCustomer(source.getReceiver()))
                .subject(source.getSubject())
                .amount(source.getAmount())
                .currency(source.getCurrency())
                .refNumber(source.getRefNumber())
                .timestamp(source.getTimestamp())
                .build();
    }

    private PaymentCustomerResponse getPaymentCustomer(Customer customer) {

        return paymentCustomerResponseMapper.convertTo(customer);
    }

    @Override
    public Payment convertFrom(PaymentResponse source, Payment destination) {
        return null;
    }

}
package com.payment.web;

import java.util.List;
import java.util.stream.Collectors;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.payment.domain.model.Account;
import com.payment.responseMapper.AccountResponseMapper;
import com.payment.service.AccountService;
import com.payment.web.response.AccountResponse;

@RestController
@RequestMapping(path = "/api/account")
@Api(tags = "account", value = "/api/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountResponseMapper accountResponseMapper;

    @ApiOperation("Get Customer's accounts")
    @GetMapping("/customer/{id}")
    public List<AccountResponse> getCustomerAccounts(@PathVariable Long id) {
        List<Account> accounts = accountService.getCustomerAccounts(id);

        return accounts.stream()
                .map(accountResponseMapper::convertTo)
                .collect(Collectors.toList());
    }

}
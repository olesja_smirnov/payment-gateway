package com.payment.web;

import java.util.List;
import java.util.stream.Collectors;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.payment.domain.model.Customer;
import com.payment.responseMapper.CustomerResponseMapper;
import com.payment.service.CustomerService;
import com.payment.web.request.CustomerRequest;
import com.payment.web.response.CustomerResponse;

@RestController
@RequestMapping(path = "/api/customer")
@Api(tags = "customer", value = "/api/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerResponseMapper customerResponseMapper;

    @ApiOperation("Get all Customers")
    @GetMapping
    public List<CustomerResponse> getCustomers() {
        List<Customer> customers = customerService.getAllCustomers();

        return customers.stream()
                .map(customerResponseMapper::convertTo)
                .collect(Collectors.toList());
    }

    @ApiOperation("Get Customer by ID")
    @GetMapping("/{id}")
    public CustomerResponse getCustomerById(@PathVariable Long id) {
        Customer customer = customerService.getCustomerById(id);

        return customerResponseMapper.convertTo(customer);
    }

    @ApiOperation("Update Customer")
    @PutMapping("/{id}")
    public CustomerResponse updateCustomer(@PathVariable Long id, @RequestBody CustomerRequest request) {
        Customer customer = customerService.updateCustomer(id, request);

        return customerResponseMapper.convertTo(customer);
    }

    @ApiOperation("Search Customer")
    @PostMapping("/search")
    public List<CustomerResponse> searchCustomer(@RequestParam String searchParam) {
        List<Customer> customers = customerService.searchCustomer(searchParam);

        return customers.stream()
                .map(customerResponseMapper::convertTo)
                .collect(Collectors.toList());
    }

}
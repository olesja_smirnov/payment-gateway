package com.payment.web.response;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import com.sun.istack.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AccountResponse {

    @NotNull
    private Long id;

    @NotNull
    private Long accountNumber;

    @NotNull
    private BigDecimal balance;

    @NotNull
    private String currency;

}
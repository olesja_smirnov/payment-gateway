package com.payment.web.response;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import com.sun.istack.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PaymentResponse {

    @NotNull
    private Long id;

    @NotNull
    private PaymentCustomerResponse payer;

    @NotNull
    private PaymentCustomerResponse receiver;

    @NotNull
    private String subject;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private String currency;

    private Long refNumber;

    @NotNull
    private LocalDateTime timestamp;

}
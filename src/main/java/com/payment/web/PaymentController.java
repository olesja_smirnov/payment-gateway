package com.payment.web;

import java.util.List;
import java.util.stream.Collectors;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.payment.domain.model.Payment;
import com.payment.responseMapper.PaymentResponseMapper;
import com.payment.service.PaymentService;
import com.payment.web.request.PaymentRequest;
import com.payment.web.response.PaymentResponse;

@RestController
@RequestMapping(path = "/api/payment")
@Api(tags = "payment", value = "/api/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentResponseMapper paymentResponseMapper;

    @ApiOperation("Get Customer's transactions by account ID")
    @GetMapping("/{accountId}")
    public List<PaymentResponse> getCustomerById(@PathVariable Long accountId) {
        List<Payment> payments = paymentService.getCustomerPayments(accountId);

        return payments.stream()
                .map(paymentResponseMapper::convertTo)
                .collect(Collectors.toList());
    }

    @ApiOperation("Arrange payment between Bank Customers")
    @PostMapping
    public void arrangeTransaction(@RequestBody PaymentRequest request) {
        paymentService.arrangeTransaction(request);
    }

}
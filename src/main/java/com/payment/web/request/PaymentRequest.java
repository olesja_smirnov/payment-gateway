package com.payment.web.request;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import com.sun.istack.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class PaymentRequest {

    @NotNull
    private Long payerId;

    @NotNull
    private Long payerAccountId;

    @NotNull
    private Long receiverId;

    @NotNull
    private Long receiverAccountId;

    @NotNull
    private String subject;

    @NotNull
    private BigDecimal amount;

    private Long refNumber;

}
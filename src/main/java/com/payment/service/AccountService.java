package com.payment.service;

import java.util.List;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.payment.domain.dao.AccountMapper;
import com.payment.domain.model.Account;

@Service
@Slf4j
public class AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Transactional
    public List<Account> getCustomerAccounts(Long id) {
        return accountMapper.getCustomerAccounts(id);
    }

}
package com.payment.service;

import static com.payment.errors.ErrorMsg.ERROR;
import static com.payment.errors.ErrorMsg.NOT_FOUND_CUSTOMER;
import static com.payment.errors.ErrorMsg.SUCCESS;

import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.payment.domain.model.Customer;
import com.payment.domain.dao.CustomerMapper;
import com.payment.errors.ResourceNotFoundException;
import com.payment.web.request.CustomerRequest;

@Service
@Slf4j
public class CustomerService {

    @Autowired
    private CustomerMapper customerMapper;

    @Transactional
    public List<Customer> getAllCustomers() {
        return customerMapper.getAllCustomers();
    }

    @Transactional
    public Customer getCustomerById(Long id) {
        Optional<Customer> customer = customerMapper.getCustomerById(id);
        if (customer.isEmpty()) {
            log.error(ERROR + NOT_FOUND_CUSTOMER);
            throw new ResourceNotFoundException(NOT_FOUND_CUSTOMER);
        }
        return customer.get();
    }

    @Transactional
    public Customer updateCustomer(Long id, CustomerRequest request) {
        Customer customer = getCustomerById(id);

        customer.setFirstName(request.getFirstName());
        customer.setLastName(request.getLastName());
        customer.setPersonalCode(request.getPersonalCode());
        customerMapper.updateCustomer(customer);
        log.info(SUCCESS + "Customer {} {} with ID {} is updated.", customer.getFirstName(), customer.getLastName(), customer.getId());

        return customer;
    }

    @Transactional
    public List<Customer> searchCustomer(String searchParam) {
        log.info("Executing Customer search request by param \"{}\"", searchParam);

        return customerMapper.searchCustomer(searchParam);
    }

}
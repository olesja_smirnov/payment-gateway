package com.payment.service;

import static com.payment.errors.ErrorMsg.SUCCESS;

import java.util.List;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.payment.domain.dao.AccountMapper;
import com.payment.domain.dao.PaymentMapper;
import com.payment.domain.model.Payment;
import com.payment.web.request.PaymentRequest;

@Service
@Slf4j
public class PaymentService {

    @Autowired
    private PaymentMapper paymentMapper;

    @Autowired
    private AccountMapper accountMapper;

    @Transactional
    public List<Payment> getCustomerPayments(Long accountId) {

        return paymentMapper.getTransactionsByCustomerAccountId(accountId);
    }

    @Transactional
    public void arrangeTransaction(PaymentRequest request) {
        paymentMapper.arrangeTransactionBetweenCustomers(request);
        accountMapper.updatePayerAccountBalance(request.getPayerAccountId(), request.getAmount());
        accountMapper.updateReceiverAccountBalance(request.getReceiverAccountId(), request.getAmount());
        log.info(SUCCESS + "Payment is arranged.");
    }

}
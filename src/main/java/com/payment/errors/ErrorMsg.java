package com.payment.errors;

public class ErrorMsg {

    public static final String ERROR = "ERROR: ";
    public static final String FAILURE = "FAILURE: ";
    public static final String NOT_FOUND_CUSTOMER = "Not found customer";
    public static final String NOT_FOUND_ACCOUNT = "Not found account";
    public static final String SUCCESS = "SUCCESS: ";

    private ErrorMsg() {

    }
}
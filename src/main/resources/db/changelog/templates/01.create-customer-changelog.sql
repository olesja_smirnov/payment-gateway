--liquibase formatted sql
--changeset olesja:create-customer-table
CREATE TABLE if not exists customer
(
    id              bigint AUTO_INCREMENT PRIMARY KEY,
    --AuditModel--
    created_at      timestamp not null default CURRENT_TIMESTAMP(),
    updated_at      timestamp not null default CURRENT_TIMESTAMP(),
    --
    first_name      varchar(64) not null,
    last_name       varchar(64) not null,
    personal_code   varchar(11) unique not null,
    CONSTRAINT customer__personal_code__unique UNIQUE (personal_code)
);

INSERT into customer values (1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Olesja', 'Smirnov', '11111111111'),
                                   (2, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'John', 'Smith', '22222222222'),
                                   (3, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Kim', 'Kardashian', '33333333333'),
                                   (4, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Steve', 'Jobs', '44444444444'),
                                   (5, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Bill', 'Gates', '55555555555'),
                                   (6, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Jack', 'Ma', '66666666666'),
                                   (7, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Kim', 'Dotcom', '77777777777'),
                                   (8, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Mark', 'Zuckerberg', '88888888888'),
                                   (9, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Ed', 'Sheeran', '99999999999'),
                                   (10, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Elon', 'Mask', '12345678912');

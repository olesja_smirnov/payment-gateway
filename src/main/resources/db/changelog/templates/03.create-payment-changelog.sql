--liquibase formatted sql
--changeset olesja:create-payment-table
CREATE TABLE if not exists payment
(
    id                      bigint AUTO_INCREMENT PRIMARY KEY,
    --AuditModel--
    created_at              timestamp not null default CURRENT_TIMESTAMP(),
    updated_at              timestamp not null default CURRENT_TIMESTAMP(),
    --
    payer_id                bigint not null,
    payer_account_id        bigint not null,
    receiver_id             bigint not null,
    receiver_account_id     bigint not null,
    subject                 varchar(255) not null,
    amount                  DECIMAL(20, 2) not null,
    currency                varchar(10) not null default 'EUR',
    ref_number              int(10),
    timestamp               timestamp not null,
    CONSTRAINT payment__payer_id__fkey FOREIGN KEY (payer_id) references customer (id),
    CONSTRAINT payment__payer_account_id__fkey FOREIGN KEY (payer_account_id) references account (id),
    CONSTRAINT payment__receiver_id__fkey FOREIGN KEY (receiver_id) references customer (id),
    CONSTRAINT payment__receiver_account_id_fkey FOREIGN KEY (receiver_account_id) references account (id)
);
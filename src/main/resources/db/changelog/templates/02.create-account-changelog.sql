--liquibase formatted sql
--changeset olesja:create-account-table
CREATE TABLE if not exists account
(
    id              bigint AUTO_INCREMENT PRIMARY KEY,
    --AuditModel--
    created_at      timestamp not null default CURRENT_TIMESTAMP(),
    updated_at      timestamp not null default CURRENT_TIMESTAMP(),
    --
    customer_id     bigint not null,
    account_number  bigint(14) unique not null,
    balance         DECIMAL(20, 2) not null,
    currency        varchar(10) not null default 'EUR',
    CONSTRAINT account__customer_id__fkey FOREIGN KEY (customer_id) references customer (id),
    CONSTRAINT account__account_number__unique UNIQUE (account_number)
);

INSERT into account values (1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1, 12345678901234, 10000, 'EUR'),
                            (2, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1, 24567123876543, 25000, 'EUR'),
                            (3, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 2, 34679123456834, 2000, 'EUR'),
                            (4, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 3, 56789345213456, 3000, 'EUR'),
                            (5, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 4, 45678901234567, 45000, 'EUR'),
                            (6, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 4, 54128794356723, 35000, 'EUR'),
                            (7, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 4, 61237657345678, 27000, 'EUR'),
                            (8, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 5, 51234876549324, 17000, 'EUR'),
                            (9, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 5, 57382956473523, 17000, 'EUR'),
                            (10, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 6, 60987612365432, 4500, 'EUR'),
                            (11, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 3, 56734512389065, 5000, 'EUR'),
                            (12, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 8, 30483746503726, 6000, 'EUR'),
                            (13, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 8, 26485623410960, 6000, 'EUR'),
                            (14, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 9, 27483947563527, 23000, 'EUR'),
                            (15, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 10, 46374859263546, 100000, 'EUR'),
                            (16, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 7, 46372956472536, 200000, 'EUR');
